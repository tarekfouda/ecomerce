import 'package:flutter/material.dart';

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      child: new ListView(

        scrollDirection: Axis.horizontal,
        children: [
           new Category(
             image_location:'images/cats/tshirt.png' ,
             image_caption: 'T-shirt',
           ),

          new Category(
            image_location:'images/cats/dress.png' ,
            image_caption: 'dress',
          ),

          new Category(
            image_location:'images/cats/jeans.png' ,
            image_caption: 'pants',
          ),

          new Category(
            image_location:'images/cats/formal.png' ,
            image_caption: 'formal',
          ),

          new Category(
            image_location:'images/cats/informal.png' ,
            image_caption: 'informal',
          ),

          new Category(
            image_location:'images/cats/shoe.png' ,
            image_caption: 'shoes',
          ),

          new Category(
            image_location:'images/cats/accessories.png' ,
            image_caption: 'others',
          ),
        ],
      ),
    );
  }
}

class Category extends StatelessWidget {
  final String image_location;
  final String image_caption;

  Category({this.image_location, this.image_caption});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: new InkWell(
        child: new Container(
          width: 80.0,
          child: new ListTile(
            onTap: () {},
            title: Image.asset(
              image_location,
              width: 40.0,
              height: 40.0,
            ),
            subtitle: new Container(
              alignment: Alignment.topCenter,
              child: new Text(image_caption),
            ),
          ),
        ),
      ),
    );
  }
}
