import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Cart_products extends StatefulWidget {
  @override
  _Cart_productsstate createState() => _Cart_productsstate();
}

class _Cart_productsstate extends State<Cart_products> {
  var products_on_the_cart = [
    {
      'name': 'Pants',
      'picture': 'images/products/pants1.jpg',
      'price': 110.0,
      'size': 'M',
      'color': 'Black',
      'Quantity': 5,
    },
    {
      'name': 'Pants',
      'picture': 'images/products/pants2.jpeg',
      'old_price': 150.0,
      'price': 110.0,
      'size': 'L',
      'color': 'Blue',
      'Quantity': 3,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: products_on_the_cart.length,
        itemBuilder: (context, index) {
          return Single_cart_product(
            cart_prod_name: products_on_the_cart[index]['name'],
            cart_prod_picture: products_on_the_cart[index]['picture'],
            cart_prod_price: products_on_the_cart[index]['price'],
            cart_prod_size: products_on_the_cart[index]['size'],
            cart_prod_color: products_on_the_cart[index]['color'],
            cart_prod_Qty: products_on_the_cart[index]['Quantity'],
          );
        });
  }
}

class Single_cart_product extends StatelessWidget {
  final cart_prod_name;
  final cart_prod_picture;
  final cart_prod_price;
  final cart_prod_size;
  final cart_prod_color;
  final cart_prod_Qty;

  Single_cart_product({
    this.cart_prod_name,
    this.cart_prod_picture,
    this.cart_prod_price,
    this.cart_prod_size,
    this.cart_prod_color,
    this.cart_prod_Qty,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: new ListTile(
//   ==========  LEADING SECTION  ========
        leading: new Image.asset(cart_prod_picture,width: 80.0,height: 80.0,),
        //  ======== THE TITLE SECTION  ========
        title: new Text(cart_prod_name),
        // ========= THE SUBTITLE SECTION =======
        subtitle: new Column(
          children: [
            new Row(
              children: [
                // ====== PRODUCT SIZE ======
                Padding(padding: EdgeInsets.all(8.0), child: new Text('size')),
                Padding(
                    padding: EdgeInsets.all(0.0),
                    child: new Text(
                      cart_prod_size,
                      style: TextStyle(color: Colors.red),
                    )),
                // ====== PRODUCT COLOR ======
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 8.0, 8.0, 8.0),
                  child: new Text('Color'),
                ),
                Padding(
                    padding: EdgeInsets.all(8.0),
                    child: new Text(
                      cart_prod_color,
                      style: TextStyle(color: Colors.red),
                    )),
              ],
            ),
            new Container(
              alignment: Alignment.topLeft,
              child: new Text(
                '\$${cart_prod_price}',
                style: TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.red),
              ),
            ),
          ],
        ),
        trailing: new Column(children: [
          new IconButton(icon: new Icon(Icons.arrow_drop_up), onPressed: (){}),
          new Text('$cart_prod_Qty'),
          new IconButton(icon: new Icon(Icons.arrow_drop_down), onPressed: (){}),
        ],),
      ),
    );
  }
}
