import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

//My own imports
import 'package:studyapp/Components/Horizontal_listview.dart';
import 'package:studyapp/Components/Products.dart';
import 'package:studyapp/Pages/cart.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    Widget image_carousel=new Container(
      height: 200.0,
      child:new Carousel(
        boxFit: BoxFit.cover,
        images: [
          new AssetImage('images/c1.jpg'),
          new AssetImage('images/IMG_1266.JPG'),
          new AssetImage('images/m1.jpeg'),
          new AssetImage('images/m2.jpg'),
          new AssetImage('images/w1.jpeg'),
          new AssetImage('images/w3.jpeg'),
          new AssetImage('images/w4.jpeg'),
        ],
        autoplay: false,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
        dotSize: 4.0,
        indicatorBgPadding: 2.0,
        dotBgColor: Colors.transparent,
      ),
    );
    return Scaffold(
      appBar: new AppBar(
        title: new Text('FashApp'),
        backgroundColor: Colors.red,
        actions: [
          IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
          IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>new Cart()));
              }),
        ],
        elevation: 0.0,
      ),
      drawer: new Drawer(
        child: new ListView(
          children: [
            //header
            new UserAccountsDrawerHeader(
              accountName: new Text('Mohammed'),
              accountEmail: new Text('mohamedmohsin737@gmail.com'),
              currentAccountPicture: new CircleAvatar(
                backgroundColor: Colors.grey,
                child: Icon(
                  Icons.person,
                  color: Colors.white,
                ),
              ),
              decoration: new BoxDecoration(
                color: Colors.red,
              ),
            ),
            //body
            InkWell(
              onTap: () {},
              child: ListTile(
                title: new Text('Home Page'),
                leading: new Icon(Icons.home,color:Colors.red,),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: new Text('My account'),
                leading: new Icon(Icons.person,color:Colors.red,),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: new Text('My orders'),
                leading: new Icon(Icons.shopping_basket,color:Colors.red,),
              ),
            ),

            InkWell(
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(builder: (context)=>new Cart()));
              },
              child: ListTile(
                title: new Text('Shopping Cart'),
                leading: new Icon(Icons.add_shopping_cart,color:Colors.red,),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: new Text('Home Page'),
                leading: new Icon(Icons.home,color:Colors.red,),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: new Text('Favourites'),
                leading: new Icon(Icons.favorite,color:Colors.red,),
              ),
            ),
            new Divider(),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: new Text('Settings'),
                leading: new Icon(Icons.settings),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: new Text('About'),
                leading: new Icon(Icons.help),
              ),
            ),
          ],
        ),
      ),
      body: new ListView(
        children: [
          //Image_carousel begins here
          image_carousel,
          //padding widget
          new Padding(padding: const EdgeInsets.all(4.0)
            ,child: Container(
                alignment: Alignment.centerLeft,
                child: new Text('Categories')),),
          //Horizontal listView begins here
          HorizontalList(),
          new Padding(padding: const EdgeInsets.all(4.0)
            ,child: Container(
                alignment: Alignment.centerLeft,
                child: new Text('Recent Products')),),
          //    ==========  GRID VIEW BEGINS HERE  ===========
          Container(
            height: 280,
            child:new Products() ,
          ),

          //=========  USING Flexible  ======
          // Flexible(child: new Products(),),
        ],
      ),
    );
  }
}
