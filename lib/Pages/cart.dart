import 'package:flutter/material.dart';


// Imprts
import 'package:studyapp/Components/cart_products.dart';
class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Cart'),
        backgroundColor: Colors.red,
        actions: [
          IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
        ],
        elevation: 0.0,
      ),
      body: new Cart_products(),
      bottomNavigationBar: new Container(
        color: Colors.white,
        child: new Row(
          children: [
            Expanded(
              child: new ListTile(
                title: new Text('Total :'),
                subtitle: new Text('\$230'),
              ),
            ),
            new  Expanded(
              child: new MaterialButton(onPressed: (){},
                child: new Text('Check Out',style: TextStyle(color: Colors.white),),
                color: Colors.red,
              ),
              
            ),
          ],
        ),
      ),
    );
  }
}
